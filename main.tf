# curl the url
data "http" "curl" {
  count = var.enabled ? 1 : 0

  url = var.source_url
}

# pipe body to file
resource "local_file" "file" {
  count = var.enabled && var.save_to_file ? 1 : 0

  filename = var.destination_file
  content  = join("", data.http.curl.*.body)
}
