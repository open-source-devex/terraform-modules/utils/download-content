module "output" {
  source = "../.."

  source_url       = "https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/release-1.5.4/config/v1.5/aws-k8s-cni.yaml"
  save_to_file     = true
  destination_file = "/tmp/out.yaml"
}
