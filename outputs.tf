output "content" {
  value = join("", data.http.curl.*.body)
}

output "filename" {
  value = join("", local_file.file.*.filename)
}
