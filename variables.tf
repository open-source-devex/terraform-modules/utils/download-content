variable "enabled" {
  description = "Whether to create the resources in the module"
  type        = bool
  default     = true
}

variable "source_url" {
  description = "The url where the content is going to be dowloaded from"
  type        = string
}

variable "save_to_file" {
  description = "Whether a file should be creeated with the downloaded content"
  type        = bool
  default     = false
}

variable "destination_file" {
  description = "The path to the file where the content will be downlaoded to"
  type        = string
  default     = null
}
