# download-file

Terraform module to download content from a URL and maybe save it to a file

## Usage

Download content and make it available as an output.
```hcl-terraform
module "download" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/utils/download-content.git?ref=v1.0.0"

  source_url = "https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/release-1.5.4/config/v1.5/aws-k8s-cni.yaml"
}

output "content" {
  value = module.download.content
}
```

Download content and also make it available in a file.
```hcl-terraform
module "output" {
  source = "../.."

  source_url       = "https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/release-1.5.4/config/v1.5/aws-k8s-cni.yaml"

  save_to_file     = true
  destination_file = "/tmp/out.yaml"
}
```

## Terraform docs

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| http | n/a |
| local | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| destination\_file | The path to the file where the content will be downlaoded to | `string` | `null` | no |
| enabled | Whether to create the resources in the module | `bool` | `true` | no |
| save\_to\_file | Whether a file should be creeated with the downloaded content | `bool` | `false` | no |
| source\_url | The url where the content is going to be dowloaded from | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| content | n/a |
| filename | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
